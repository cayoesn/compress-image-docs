# coding: utf-8
# Importa as bibliotecas PIL (Python Imaging Library), os (Biblioteca para usar arquivos) e shutil (Biblioteca usada para substituir Arquivos)
import PIL
from PIL import Image
import os
from shutil import move
import time
from time import strftime
from datetime import datetime

# Variavel Global para mostrar a quantidade de imagens em cada linha
cont = 1


class ComprimiImagem:
    # Metodo construtor da Classe, instanciando as 3 variaveis principais (caminhoOrigem = Diretorio das Imagens,
    # manterNome = SE DESEJA SUBSTITUIR O ARQUIVO, OU CRIAR OUTRO COM '_COMPRIMIDO' NA FRENTE),
    # tamanhoMax = Tamanho Maximo dos arquivos para tentar comprimi-los)
    def __init__(self, caminhoOrigem, manterNome, tamanhoMax):
        self.caminhoOrigem = caminhoOrigem
        self.tamanhoMax = tamanhoMax
        self.manterNome = manterNome

    def comprimirImage(self):
        # Instancia a variavel Global e inicializa as variaveis usadas no escopo
        global cont
        caminhoImagem = self.caminhoImagem
        caminhoDestino = self.caminhoDestino
        manterNome = self.manterNome
        tamanhoMax = self.tamanhoMax

        # Abre a imagem e pega os dados principais dela, nome, tamanho, extensao, etc.
        imagem = Image.open(caminhoImagem)
        widht, height = imagem.size
        widhtPermitido = 800
        heightPermitido = 600
        extensao = caminhoImagem.split(".")[len(caminhoImagem.split(".")) - 1]
        novoNome = caminhoImagem.split(
            "\\")[len(caminhoImagem.split("\\")) - 1]
        novoNome = caminhoImagem.split(
            "\\")[len(caminhoImagem.split("\\")) - 1]
        novoNome = novoNome.replace(str(".") + str(extensao), "")
        novoNome = novoNome + "_Comprimido." + extensao

        tamanhoOriginal = os.path.getsize(caminhoImagem)
        tamanhoFinal = 0
        compressNovamente = 1

        # Loop para tentar comprimir mais de uma vez, se a imagem continuar grande apos a primeira compressao
        while(compressNovamente == 1):
            # Verifica as medidas da imagem
            if (widht > widhtPermitido or height > heightPermitido):
                if(widht > widhtPermitido):
                    height = int(
                        height * ((widhtPermitido * 100) / widht) / 100)
                    widht = widhtPermitido
                else:
                    widht = int(
                        widht * ((heightPermitido * 100) / height) / 100)
                    height = heightPermitido

            # Salva a imagem, separado por PNG e JPG'S, metodo de reducao diferente
            imagem = imagem.resize((widht, height), PIL.Image.ANTIALIAS)
            if(extensao == "png"):
                imagem.save(caminhoDestino + novoNome,
                            format="PNG", optimize=True)
            else:
                imagem.save(caminhoDestino + novoNome,
                            format="JPEG", optimize=True, quality=80)

            # Verifica o tamanho da imagem, e as medidas para se necessario comprimir de novo no Loop
            tamanhoFinal = os.path.getsize(caminhoDestino + novoNome)
            if(tamanhoFinal > tamanhoMax):
                widhtPermitido -= 80
                heightPermitido -= 60

                if (widhtPermitido < 640 or heightPermitido < 480):
                    if(manterNome == "true" and tamanhoFinal > tamanhoOriginal):
                        os.remove(caminhoDestino + novoNome)
                    elif (manterNome == "true" and tamanhoFinal <= tamanhoOriginal):
                        move(caminhoDestino + novoNome, caminhoImagem)

                    compressNovamente = 0
                else:
                    compressNovamente = 1
            else:
                if(manterNome == "true" and tamanhoFinal > tamanhoOriginal):
                    os.remove(caminhoDestino + novoNome)
                elif (manterNome == "true" and tamanhoFinal <= tamanhoOriginal):
                    move(caminhoDestino + novoNome, caminhoImagem)

                compressNovamente = 0

        # Verificacao somente para mostrar o nome correto na saida do codigo
        if(manterNome == "true"):
            nomeArquivo = caminhoImagem
        else:
            nomeArquivo = caminhoDestino + novoNome

        # Verificacao somente para mostrar a saida do codigo
        if(tamanhoFinal < tamanhoOriginal):
            print(str(cont) + " - Imagem: " + nomeArquivo + " | Tamanho (Antes): " +
                  str(tamanhoOriginal / 1000) + " Kbs - Tamanho (Depois): " + str(tamanhoFinal / 1000) + " Kbs")
        else:
            print(str(cont) + " - Imagem: " + nomeArquivo + " | Tamanho (Antes): " +
                  str(tamanhoOriginal / 1000) + " Kbs - Nao foi alterado")

        # Soma necessaria para mostrar o numero de cada imagem na saida do codigo
        cont = cont + 1

    # Funcao 'Principal' utilizada para comprimir as imagens e salva-las no mesmo diretorio
    def comprimirProprioDiretorio(self):
        # Inicializa as variaveis usadas no escopo
        diretorio = self.caminhoOrigem
        tamanhoMax = self.tamanhoMax

        # Loop no diretorio e sub-diretorios passado pelo parametro 'caminhoOrigem'
        for r, d, f in os.walk(diretorio):
            for file in f:
                nomeArquivo = os.path.join(r, file)
                extensao = nomeArquivo.split(
                    ".")[len(nomeArquivo.split(".")) - 1]

                caminhoOrigem = os.path.join(r, file).split(
                    "\\")[len(os.path.join(r, file).split("\\")) - 1]
                caminhoOrigem = os.path.join(
                    r, file).replace(caminhoOrigem, "")
                tamanhoOriginal = os.path.getsize(nomeArquivo)

                # Instancia novas variaveis para a funcao 'comprimirImage()', com o self
                self.caminhoImagem = nomeArquivo
                self.caminhoDestino = diretorio

                # Pega a data de CRIAÇÃO do Arquivo para comparar com a Data Atual
                dataCriacao = os.path.getctime(nomeArquivo)
                dataCriacao = time.gmtime(dataCriacao)
                dataCriacao = strftime("%d/%m/%Y", dataCriacao)
                dataAtual = datetime.now()
                dataAtual = dataAtual.strftime("%d/%m/%Y")

                # Verifica a extensao da imagem (se permitida), o tamanho se e maior que o parametro 'tamanhoMax' e a Data de CRIAÇÃO se é igual a Data Atual
                # and (dataCriacao == dataAtual):
                if(extensao == "jpg" or extensao == "jpeg" or extensao == "png") and (tamanhoOriginal > tamanhoMax):
                    ComprimiImagem.comprimirImage(self)


# Instancia a funcao com os parametros e chama a funcao 'comprimirProprioDiretorio()'
ComprimiImagem("C:\\Users\\ACTROM_Cayo\\Downloads\\Nova pasta\\",
               "true", 100000).comprimirProprioDiretorio()
