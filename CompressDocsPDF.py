# coding: utf-8
# Importa as bibliotecas datetime (Trabalhar com Data), os (Biblioteca para usar arquivos), shutil (Biblioteca usada para substituir Arquivos), pylovepdf.ilovepdf (Biblioteca do iLovePDF para Python)
from datetime import datetime
import os
from pylovepdf.ilovepdf import ILovePdf
from shutil import move
import time
from time import strftime

# Variavel Global para mostrar a quantidade de documentos em cada linha
cont = 1


class ComprimiDocsPDF:
    # Metodo construtor da Classe, instanciando as 3 variaveis principais (caminhoOrigem = Diretorio das Imagens,
    # manterNome = SE DESEJA SUBSTITUIR O ARQUIVO, OU CRIAR OUTRO COM "_COMPRIMIDO" NA FRENTE),
    # tamanhoMax = Tamanho Maximo dos arquivos para tentar comprimi-los)
    def __init__(self, caminhoOrigem, manterNome):
        self.caminhoOrigem = caminhoOrigem
        self.manterNome = manterNome

    def comprimirDocumentoPDF(self):
        # Instancia a variavel Global e inicializa as variaveis usadas no escopo
        try:
            global cont
            caminhoDocumento = self.caminhoDocumento
            caminhoDestino = self.caminhoDestino
            manterNome = self.manterNome

            nomeDocumento = os.path.join(caminhoDocumento).split("\\")
            nomeDocumento = nomeDocumento[len(nomeDocumento)-1]

            dataAtual = datetime.now()
            dataAtual = dataAtual.strftime("%d-%m-%Y")
            extensao = nomeDocumento.split(
                ".")[len(nomeDocumento.split(".")) - 1]
            novoNome = nomeDocumento.replace("." + str(extensao), "")
            novoNome = novoNome + "_compress_" + \
                str(dataAtual) + "." + extensao
            tamanhoOriginal = os.path.getsize(caminhoDocumento)
            tamanhoFinal = 0

            ilovepdf = ILovePdf(
                "project_public_378af5a0bbf54cc2629ada54f95a06b9_CICDT62dca4e6dfb091561d1a1d5a01c83cfc", verify_ssl=True)
            task = ilovepdf.new_task("compress")
            task.add_file(caminhoDocumento)
            task.compression_level = "extreme"
            task.set_output_folder(caminhoDestino)
            task.output_filename = "actrom_comprimir"
            task.execute()
            task.download()
            # task.delete_current_task()

            # Verificacao somente para mostrar o nome correto na saida do codigo
            print(novoNome)
            if(manterNome == "true"):
                nomeArquivo = nomeDocumento
                move(caminhoDestino + "comprimir.pdf_actrom_comprimir.pdf",
                     caminhoDocumento)
            else:
                nomeArquivo = novoNome

            tamanhoFinal = os.path.getsize(caminhoDocumento)
            # Verificacao somente para mostrar a saida do codigo
            if(tamanhoFinal < tamanhoOriginal):
                print(str(cont) + " - Documento (PDF): " + nomeArquivo + " | Tamanho (Antes): " +
                      str(tamanhoOriginal / 1000) + " Kbs - Tamanho (Depois): " + str(tamanhoFinal / 1000) + " Kbs")
            else:
                print(str(cont) + " - Documento (PDF): " + nomeArquivo + " | Tamanho (Antes): " +
                      str(tamanhoOriginal / 1000) + " Kbs - Nao foi alterado")

            cont = cont + 1
        finally:
            # Soma necessaria para mostrar o numero de cada documento na saida do codigo
            print("Ocorreu um problema na Compressão do PDF")
            return 0

    # Funcao "Principal" utilizada para comprimir as documentos e salva-las no mesmo diretorio
    def comprimirProprioDiretorio(self):
        # Inicializa as variaveis usadas no escopo
        diretorio = self.caminhoOrigem

        # Loop no diretorio e sub-diretorios passado pelo parametro "caminhoOrigem"
        for r, d, f in os.walk(diretorio):
            for file in f:
                nomeArquivo = os.path.join(r, file)
                extensao = nomeArquivo.split(
                    ".")[len(nomeArquivo.split(".")) - 1]

                caminhoOrigem = os.path.join(r, file).split(
                    "\\")[len(os.path.join(r, file).split("\\")) - 1]
                caminhoOrigem = os.path.join(
                    r, file).replace(caminhoOrigem, "")

                # Instancia novas variaveis para a funcao "comprimirImage()", com o self
                self.caminhoDocumento = nomeArquivo
                self.caminhoDestino = diretorio

                # Pega a data de CRIAÇÃO do Arquivo para comparar com a Data Atual
                dataCriacao = os.path.getctime(nomeArquivo)
                dataCriacao = time.gmtime(dataCriacao)
                dataCriacao = strftime("%d/%m/%Y", dataCriacao)
                dataAtual = datetime.now()
                dataAtual = dataAtual.strftime("%d/%m/%Y")

                # Verifica a extensao da documento (se permitida), e a Data de CRIAÇÃO se é igual a Data Atual
                if(extensao == "pdf") and (dataCriacao == dataAtual):
                    ComprimiDocsPDF.comprimirDocumentoPDF(self)


# Instancia a funcao com os parametros e chama a funcao "comprimirProprioDiretorio()"
ComprimiDocsPDF("C:\\Users\\ACTROM_Cayo\\Downloads\\cl_0000 - Copia\\",
                "true").comprimirProprioDiretorio()
